## [:< ##

# name  = discover.load_update_notify_rqs

my $request_ref    = <discover.host-status-requests> = {};   ##  resetting  ##
my $p7_directories = <system.path.zenka-dirs>;
my $zenka_name_re  = <regex.base.usr_str>;

my $zenka_path = catdir( $p7_directories->{'var_P7'},
    <system.zenka.name>, qw| notify-reqs | );

if ( not -d $zenka_path ) {
    <[base.log]>->( 2, 'no config directory, skipped loading ..,' );
    return undef;
}

<[base.log]>->( 1, 'loading update notification requests' );

<[base.s_warn]>->( "no read access on '%s'", $zenka_path )
    if not -r $zenka_path;

foreach my $file ( <[file.all_files]>->($zenka_path)->@* ) {
    next if $file !~ m|/([A-Z2-7]{13})\.($zenka_name_re)$|;
    my $hostname_L13 = ${^CAPTURE}[0];
    my $zenka        = ${^CAPTURE}[1];
    <[base.logs]>->(
        2, ': loading : host update request [%s] .,',
        $hostname_L13
    ) if not exists <discover.resend-host-reqs>->{$hostname_L13};
    <[base.logs]>->( 2, "          : zenka '%s' .,", $zenka );
    my $hostkey;
    my $key_ref = <[file.slurp]>->($file);
    if ( ref $key_ref ne qw| SCALAR | ) {
        <[base.s_warn]>
            ->( "update request load error ['%s']", $hostname_L13 );
        next;
    }
    chomp $key_ref->$*;
    if ( index( $key_ref->$*, qw| : |, 0 ) == -1 ) {    ## no key available ##
        <[base.log]>->( 2, ': :. no host key.,' );
    }
    ( my $name, my $hostkey ) = split qw| : |, $key_ref->$*, 2;
    my $key_bin;
    if ( defined $hostkey ) {
        if ( defined $hostkey and length($hostkey) != 52 ) {
            <[base.s_warn]>->(
                "resend request encoded host key has wrong length ['%s']",
                $hostname_L13
            );
            next;
        } elsif ( defined $hostkey and $hostkey !~ m|^[A-Z2-7]{52}$| ) {
            <[base.s_warn]>->(
                "resend request encoded host key is not in BASE 32 ['%s']",
                $hostname_L13
            );
            next;
        }
        $key_bin = Crypt::Misc::decode_b32r($hostkey);
        if ( not defined $key_bin ) {
            <[base.s_warn]>->(
                "resend request [%s] host key B32 decoding failed",
                $hostname_L13
            );
            next;
        }
    }

    my @notify_req_zenki;

    if ( exists <discover.host-status-requests>->{$hostname_L13} ) {
        @notify_req_zenki = uniq $zenka,
            <[base.sort]>->(
            <discover.host-status-requests>->{$hostname_L13}->{'zenki'}->@* );
    } else {
        push @notify_req_zenki, $zenka;
    }

    $request_ref->{$hostname_L13} = {
        qw| host |     => $name,
        qw| zenki |    => \@notify_req_zenki,
        qw| host-key | => $key_bin
    };
}

my $num_reqs = keys $request_ref->%*;

if ( not $num_reqs ) {
    <[base.log]>->( 1, ':. nothing saved.,' );
} else {
    <[base.logs]>->(
        ':. %d host update request%s configured .,',
        $num_reqs, <[base.cnt_s]>->($num_reqs)
    );
}

return $request_ref;    ##[ returning href of loaded resend requests ]##

#,,.,,,.,,,,.,,.,,,..,,..,,.,,,..,.,.,.,,,..,,..,,...,.,.,..,,...,.,,,..,,...,
#VSGKHQZCAXIC5O2GVVIDLUNVFGCZ5VWFPN2HMRTEOITAHWD2KEWTUJRNKDZ32S26O5D75HR2XZ7VK
#\\\|PYNDVJSWLRLT7ZWX4GZZJNIGDK2NCYNI2FFXQVZKHRIXYFAXXKN \ / AMOS7 \ YOURUM ::
#\[7]MKSR5WMOLKMDLUW6H67EEYRKTG4GBY3TR4T5ZICETXWRGHUWMGDA 7  DATA SIGNATURE ::
#:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
